import json
import math

import googlemaps
import pandas

from ade.datastructures import COMMUTE, HEADER


class AddressDataEnricher(object):
    def __init__(self, google_api_key=None, dep_time_to_location=None, dep_time_from_location=None, source_file=None, debug=False, location_address=None):
        self.debug = debug
        self.api_key = google_api_key
        self.departure_time_to_location = dep_time_to_location
        self.departure_time_from_location = dep_time_from_location
        self.location_address = location_address
        self.commute = COMMUTE
        self.data_frame_header = HEADER
        self.gmaps_api_client = googlemaps.Client(key=google_api_key)
        self.data_src_csv = self.read_file(source_file)
        self.data_src_obj = self.objectify_csv(csv_content=self.data_src_csv)

    @staticmethod
    def read_file(filename=None):
        with open(filename, 'r', encoding="utf-8") as f:
            cont = f.readlines()
        f.close()
        return cont

    @staticmethod
    def write_file(filename=None, content=None):
        try:
            with open(filename, 'w', encoding='utf-8', newline='') as f:
                f.write(content)
            f.close()
            return True
        except Exception as e:
            print(e)
            return False

    def objectify_csv(self, csv_content=None):
        address_list = []
        for line in csv_content:
            address = {
                "daysatlocation": line.split(';')[0].strip(),
                "location": line.split(';')[1].strip(),
                "Country": line.split(';')[2].strip(),
                "Street": line.split(';')[3].strip(),
                "Postcode": line.split(';')[4].strip(),
                "City": line.split(';')[5].strip(),
                "Commute": ""
            }
            address_list.append(address)
        if self.debug:
            self.write_file('ao_debug.json', json.dumps(address_list, ensure_ascii=False))
        return address_list

    def days_at_location_ceil(self, address_list=None):
        for dt_set in address_list:
            daysperweek = dt_set['daysatlocation'].replace(',', '.')
            dt_set['daysatlocation_ceiling'] = math.ceil(float(daysperweek))
        if self.debug:
            self.write_file('ao_ceil.json', json.dumps(address_list, ensure_ascii=False))
        return address_list

    def commuting_directions(self, address_list=None):
        for dset in address_list:
            commute = json.loads(COMMUTE)
            location_addr = self.location_address
            if dset['daysatlocation_ceiling'] > 0:
                location_home = dset['Country'] + ',' + dset['Street'] + ',' + dset['Postcode'] + ',' + dset['City']
                location_work = location_addr['Country'] + ',' + location_addr['Street'] + ',' + location_addr[
                    'Postcode'] + ',' + location_addr['City']
                commute['home_addr'] = location_home
                commute['location_addr'] = location_work
                for poss in commute["commuting_possibilities"]:
                    if poss['type'] == 'driving':
                        try:
                            print("Getting driving directions for address: " + location_home)
                            if self.departure_time_to_location:
                                to_work_drive = self.gmaps_api_client.directions(location_home,
                                                                                 location_work,
                                                                                 departure_time=self.departure_time_to_location)
                            else:
                                to_work_drive = self.gmaps_api_client.directions(location_home,
                                                                                 location_work)
                            if self.departure_time_from_location:
                                from_work_drive = self.gmaps_api_client.directions(location_work,
                                                                                   location_home,
                                                                                   departure_time=self.departure_time_from_location)
                            else:
                                from_work_drive = self.gmaps_api_client.directions(location_work,
                                                                                   location_home)
                            poss['distance_to_location_avg'] = to_work_drive[0]['legs'][0]['distance']['value']
                            poss['distance_to_location_peak'] = to_work_drive[0]['legs'][0]['distance']['value']
                            poss['duration_to_location_avg'] = to_work_drive[0]['legs'][0]['duration']['value']
                            if self.departure_time_to_location:
                                poss['duration_to_location_peak'] = to_work_drive[0]['legs'][0]['duration_in_traffic'][
                                    'value']
                            poss['distance_from_location_avg'] = from_work_drive[0]['legs'][0]['distance']['value']
                            poss['distance_from_location_peak'] = from_work_drive[0]['legs'][0]['distance']['value']
                            poss['duration_from_location_avg'] = from_work_drive[0]['legs'][0]['duration']['value']
                            if self.departure_time_from_location:
                                poss['duration_from_location_peak'] = from_work_drive[0]['legs'][0]['duration_in_traffic'][
                                    'value']
                        except Exception as e:
                            print("Getting driving directions for address: " + location_home + " but there was an error")
                            print(e)
                    elif poss['type'] == 'bicycling' or poss['type'] == 'transit':
                        try:
                            print("Getting bicycling/transit directions for address: " + location_home)
                            to_work = self.gmaps_api_client.directions(location_home, location_work, mode=poss['type'])
                            form_work = self.gmaps_api_client.directions(location_work, location_home,
                                                                         mode=poss['type'])
                            poss['distance_to_location'] = to_work[0]['legs'][0]['distance']['value']
                            poss['duration_to_location'] = to_work[0]['legs'][0]['duration']['value']
                            poss['distance_from_location'] = form_work[0]['legs'][0]['distance']['value']
                            poss['duration_from_location'] = form_work[0]['legs'][0]['duration']['value']
                        except Exception as e:
                            print(
                                "Getting bicycling/transit directions for address: " + location_home +
                                "but there was an error")
                            print(e)

            dset['Commute'] = commute
        if self.debug:
            self.write_file('ao_commute.json', json.dumps(address_list, ensure_ascii=False))
        return address_list

    def create_data_frame(self, address_list_complete=None):
        df = pandas.DataFrame(columns=self.data_frame_header)
        for dataset in address_list_complete:
            try:
                data_row = [
                    dataset['daysatlocation'],
                    dataset['daysatlocation_ceiling'],
                    dataset['location'],
                    dataset['Country'],
                    dataset['Street'],
                    dataset['Postcode'],
                    dataset['City']
                ]
                for poss in dataset['Commute']['commuting_possibilities']:
                    if poss['type'] == 'bicycling' or poss['type'] == 'transit':
                        data_row.append(poss['distance_to_location'])
                        data_row.append(poss['duration_to_location'])
                        data_row.append(poss['distance_from_location'])
                        data_row.append(poss['duration_from_location'])
                    elif poss['type'] == 'driving':
                        data_row.append(poss['distance_to_location_avg'])
                        data_row.append(poss['distance_to_location_peak'])
                        data_row.append(poss['duration_to_location_avg'])
                        data_row.append(poss['duration_to_location_peak'])
                        data_row.append(poss['distance_from_location_avg'])
                        data_row.append(poss['distance_from_location_peak'])
                        data_row.append(poss['duration_from_location_avg'])
                        data_row.append(poss['duration_from_location_peak'])
                df.loc[len(df)] = data_row
            except Exception as e:
                print(e)
        if self.debug:
            self.write_file('dataframe_complete.txt', df.to_string())
        return df