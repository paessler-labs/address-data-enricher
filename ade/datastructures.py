COMMUTE = '''{

    "home_addr": "",
    "location_addr": "",
    "commuting_possibilities": [
        {
            "type": "bicycling",
            "distance_to_location": "0",
            "duration_to_location": "0",
            "distance_from_location": "0",
            "duration_from_location": "0"
        },
        {
            "type": "transit",
            "distance_to_location": "0",
            "duration_to_location": "0",
            "distance_from_location": "0",
            "duration_from_location": "0"
        },
        {
            "type": "driving",
            "distance_to_location_avg": "0",
            "distance_to_location_peak": "0",
            "duration_to_location_avg": "0",
            "duration_to_location_peak": "0",
            "distance_from_location_avg": "0",
            "distance_from_location_peak": "0",
            "duration_from_location_avg": "0",
            "duration_from_location_peak": "0"
        }
    ]
}'''

HEADER = (
    'days at location',
    'days at location (ceil)',
    'location',
    'country',
    'street',
    'postcode',
    'city',
    'commute_bike distance to location (m)',
    'commute_bike duration to location (s)',
    'commute_bike distance to home (m)',
    'commute_bike duration to home (s)',
    'commute_transit distance to location avg (m)',
    'commute_transit duration to location peak (s)',
    'commute_transit distance to home avg (m)',
    'commute_transit duration to home peak (s)',
    'commute_driving distance to location avg (m)',
    'commute_driving duration to location peak (s)',
    'commute_driving distance to home avg (m)',
    'commute_driving duration to home peak (s)',
    'commute_driving distance to location avg (m)',
    'commute_driving duration to location peak (s)',
    'commute_driving distance to home avg (m)',
    'commute_driving duration to home peak (s)'
)
