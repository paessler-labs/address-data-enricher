# Address data enricher for commuting data

## Intention  
This script takes basic address data and enriches it with daily commuting data (pulled from Google Maps API).
Commuting data is pulled for bike, public transport and driving you also can provide departure times to see how data changes with different traffic.

## Prerequisites
- Python 3.x
- [Google Maps API Key](https://developers.google.com/maps/gmp-get-started#get-api-key)
- csv address data in the [format described below](#input-dataformat)
- API Key has to have access to the [Directions API](https://developers.google.com/maps/documentation/directions/start)

## Usage  
1. clone the repo
2. make sure you have [pip](https://pypi.org/project/pip/) available on your preferred shell
3. cd into the repo and install requirements ( ````pip install -r requirements.txt```` )
4. (if not present create a file called ````conf.py```` on the same level as ````enrich_me.py````)
5. in the file ````conf.py```` adjust the LOCATION_ADDRESS (lat/lon are optional) 
6. create a  ````csv file```` for input data ([Input dataformat]((#input-dataformat)))
7. run the script with ````python ./enrich_me.py -h```` to get all available parameters
8. the script will create three output files in different formats (JSON, CSV, Pandas Dataframe)

## Parameters
````bash
usage: enrich_me.py [-h] --api-key API_KEY --input-file INPUT_FILE
                    [--dep-to DEP_TO] [--dep-from DEP_FROM] [--debug]

Commuting data enrichment for simple address datasets

optional arguments:
  -h, --help            show this help message and exit
  --api-key API_KEY     API Key for the Google Maps API
  --input-file INPUT_FILE
                        Path to csv file which contains the raw data
  --dep-to DEP_TO       Departure time from your home in unix epoch
  --dep-from DEP_FROM   Departure time from your location (e.g. office) in
                        unix epoch
  --debug               If the debug flag is set all intermediatesteps wil
                        create json files with the dataset after each action

````
HINT: There is [a converter](https://www.epochconverter.com/) to convert to/from unix epoch.
## Some explanations
When setting --dep-to or --dep-from (or both) the script will take traffic information on your commute in account.


## Input dataformat
The input data format is csv with semicolons instead of commas:
````csv
DAYS AT LOCATION;LOCATION NAME;COUNTRY;STREET INCLUDING NUMBER;POST CODE;CITY 
````
#### Meaning  
DAYS AT LOCATION -> How many days per week do you spend at the given location  
LOCATION NAME -> Some name, e.g Office, Work or School

#### Datatypes  
DAYS AT LOCATION -> Float e.g. 1,5  
LOCATION NAME;COUNTRY;STREET INCLUDING NUMBER;POST CODE;CITY -> String

#### Example Dataformat
````csv
5,00 ;PHQ;Deutschland;Johannisstrasse 55;90419;Nürnberg
5,00 ;PHQ;Deutschland;Pirckheimerstraße 116;90409;Nürnberg
````

## Dataset formats
Units used in the output data format are seconds (s) and metres (m). 

JSON
````json
{
        "daysatlocation": "5,00",
        "location": "PHQ",
        "Country": "Deutschland",
        "Street": "Pirckheimerstraße 116",
        "Postcode": "90409",
        "City": "Nürnberg",
        "Commute":
        {
            "home_addr": "Deutschland,Pirckheimerstraße 116,90409,Nürnberg",
            "location_addr": "Deutschland,Thurn-und-Taxis Strasse 14,90411,Nürnberg",
            "commuting_possibilities":
            [
                {
                    "type": "bicycling",
                    "distance_to_location": 4514,
                    "duration_to_location": 998,
                    "distance_from_location": 4599,
                    "duration_from_location": 921
                },
                {
                    "type": "transit",
                    "distance_to_location": 4734,
                    "duration_to_location": 1371,
                    "distance_from_location": 4852,
                    "duration_from_location": 1505
                },
                {
                    "type": "driving",
                    "distance_to_location_avg": 4513,
                    "distance_to_location_peak": 4513,
                    "duration_to_location_avg": 598,
                    "duration_to_location_peak": 694,
                    "distance_from_location_avg": 4590,
                    "distance_from_location_peak": 4590,
                    "duration_from_location_avg": 631,
                    "duration_from_location_peak": 922
                }
            ]
        },
        "daysatlocation_ceiling": 5
    }
````
CSV
````csv
,days at location,days at location (ceil),location,country,street,postcode,city,commute_bike distance to location (m),commute_bike duration to location (s),commute_bike distance to home (m),commute_bike duration to home (s),commute_transit distance to location avg (m),commute_transit duration to location peak (s),commute_transit distance to home avg (m),commute_transit duration to home peak (s),commute_driving distance to location avg (m),commute_driving duration to location peak (s),commute_driving distance to home avg (m),commute_driving duration to home peak (s),commute_driving distance to location avg (m),commute_driving duration to location peak (s),commute_driving distance to home avg (m),commute_driving duration to home peak (s)
0,"5,00",5,PHQ,Deutschland,Pirckheimerstraße 116,90409,Nürnberg,4514,998,4599,921,4734,1371,4852,1505,4513,4513,598,694,4590,4590,631,922
````
Pandas Dataframe
````text
  days at location days at location (ceil) location      country                  street postcode      city commute_bike distance to location (m) commute_bike duration to location (s) commute_bike distance to home (m) commute_bike duration to home (s) commute_transit distance to location avg (m) commute_transit duration to location peak (s) commute_transit distance to home avg (m) commute_transit duration to home peak (s) commute_driving distance to location avg (m) commute_driving duration to location peak (s) commute_driving distance to home avg (m) commute_driving duration to home peak (s) commute_driving distance to location avg (m) commute_driving duration to location peak (s) commute_driving distance to home avg (m) commute_driving duration to home peak (s)
0             5,00                       5      PHQ  Deutschland   Pirckheimerstraße 116    90409  Nürnberg                                  4514                                   998                              4599                               921                                         4734                                          1371                                     4852                                      1505                                         4513                                          4513                                      598                                       694                                         4590                                          4590                                      631                                       922
````

## Feedback
Feedback welcome :)

### Contributors
Maximilian Funke, Paessler AG  
Konstantin Wolff, Paessler AG  

