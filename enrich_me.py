import sys
import json
import argparse

from ade.ade import AddressDataEnricher

# Some error handling in respect to the config file
try:
    from conf import LOCATION_ADDR
except ModuleNotFoundError as e:
    print("Please add a config file called conf.py on the same level as the enrich_me.py file. See readme for details")
    sys.exit(1)

if LOCATION_ADDR['Country'] == 'COUNTRY' or LOCATION_ADDR['Street'] == 'STREET STREET NO':
    print('It seems you are using the example config, please adjust the values in your conf.py file!')
    sys.exit(1)


parser = argparse.ArgumentParser(description='Commuting data enrichment for simple address datasets')
parser.add_argument('--api-key', help='API Key for the Google Maps API', required=True)
parser.add_argument('--input-file', help='Path to csv file which contains the raw data', required=True)
parser.add_argument('--dep-to', help='Departure time from your home in unix timeticks')
parser.add_argument('--dep-from', help='Departure time from your location (e.g. office) in unix timeticks')
parser.add_argument('--debug', action='store_true', required=False, help='If the debug flag is set all intermediate'
                                                                         'steps wil create json files with the dataset '
                                                                         'after each action')
args = parser.parse_args()


if __name__ == '__main__':
    ade = AddressDataEnricher(google_api_key=args.api_key,
                              dep_time_to_location=args.dep_to,
                              dep_time_from_location=args.dep_from,
                              source_file=args.input_file,
                              debug=args.debug,
                              location_address=LOCATION_ADDR)
    ceil_list = ade.days_at_location_ceil(address_list=ade.data_src_obj)
    commute_list = ade.commuting_directions(address_list=ceil_list)
    dataframe = ade.create_data_frame(address_list_complete=commute_list)
    # creating output csv, json and plain dataframe
    ade.write_file('enriched_address_data.csv', dataframe.to_csv())
    ade.write_file('enriched_address_data.json', json.dumps(commute_list, ensure_ascii=False))
    ade.write_file('enriched_address_data.txt', dataframe.to_string())

